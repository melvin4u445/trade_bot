# Strategy2: Sell/Buy Litecoin when the price goes Below/Above the expected market price

import actions, strategy1, config
import time
from decimal import Decimal

product = actions.product
my_market_orders = []
sequence_number_counter = Decimal('1')  # For tracking each transaction

# Fetch values from the settings file
initial_market_price = Decimal(str(config.config_data['strategy2']['initial_market_price']))
allowed_difference = Decimal(str(config.config_data['strategy2']['allowed_difference']))  # Buy/Sell if initial_market_price goes below allowed_price
stop_placing_buy_limit_orders = int(config.config_data['strategy2']['stop_placing_buy_limit_orders'])  # Maximum number of times a buy limit order is placed after market_down()
stop_placing_sell_limit_orders = int(config.config_data['strategy2']['stop_placing_sell_limit_orders'])  # Maximum number of times a sell limit order is placed after market_up()

def initial_run():
    try:
        global initial_market_price
        account_info = actions.account_value()

        # Fetch initial_market_price from the api, if the settings file doesn't have a modified value
        if initial_market_price == 0:
            initial_market_price = account_info['current_market_price']
        actions.print_log("initial_market_price: ", initial_market_price, "market_down at: ", initial_market_price - allowed_difference, "market_up at: ", initial_market_price + allowed_difference)
    except:
        actions.print_log("initial_run2 exception")
        actions.print_exception()

def execute_strategy():
    try:
        account_info = actions.account_value()
        current_market_price = account_info['current_market_price']

        # Check if market price went down
        if current_market_price < initial_market_price - allowed_difference:
            market_down(current_market_price)  # Sell currency1

        # Check if market price went up
        elif current_market_price > initial_market_price + allowed_difference:
            market_up(current_market_price)  # Buy currency1
    except:
        actions.print_log("execute_strategy2 exception")
        actions.print_exception()


# If market price goes down, sell Litecoin
def market_down(current_market_price):
    try:
        global initial_market_price, stop_placing_buy_limit_orders
        if actions.account_value()['currency1_total'] != 0:
            # Log allowed difference and initial and current market price for later reference
            actions.print_log("initial_market_price:                      ", initial_market_price)
            actions.print_log("current_market_price:                      ", current_market_price)
            actions.print_log("initial_market_price - allowed_difference: ", initial_market_price - allowed_difference)

            actions.print_log("Market Down. Sell currency1")
            actions.print_log("Cancelling currency1 held limit orders(if any)")
            actions.cancel_multiple_limit_orders(strategy1.my_reds)

            currency1_available = actions.account_value()['currency1_available']
            currency1_available = actions.truncate(currency1_available, 8)
            actions.print_log("currency1_available = ", currency1_available)
            is_selling = False  # To update account info if currency1 is sold

            # Coinbase does not allow sell a huge number of coins in one transaction
            # and hence it should be split into smaller portions

            # Keep selling only if the market price is still under initial market price - allowed_difference

            while (currency1_available > 0.1) and (actions.account_value()['current_market_price'] <= (initial_market_price - allowed_difference)):
                is_selling = True
                # Fetch the market_down_sell_size value from the settings file
                market_down_sell_size = Decimal(str(config.config_data['strategy2']['market_down_sell_size']))
                if currency1_available <= market_down_sell_size:
                    response = actions.execute_market_order('sell', product, currency1_available)
                else:
                    response = actions.execute_market_order('sell', product, market_down_sell_size)
                track_order(response)
                currency1_available = Decimal(str(actions.fetch_account_by_id(actions.currency1_account_id)['available']))
            if is_selling:
                time.sleep(1)  # To give time for the server to tally the balances
                account_info = actions.account_value()
                initial_market_price = account_info['current_market_price']
                actions.print_log("initial_market_price: ", initial_market_price, "market_down at: ", initial_market_price - allowed_difference, "market_up at: ", initial_market_price + allowed_difference)

                # Place buy limit orders again with available currency2
                if stop_placing_buy_limit_orders > 0:  # To stop loss from overdoing this action
                    actions.print_log("Placing Buy Limit orders again after Market Sell. Number: ", stop_placing_buy_limit_orders)
                    strategy1.fetch_greens_and_reds()
                    strategy1.place_initial_buy_order(strategy1.initial_buy_quantity, strategy1.initial_buy_size)  # Parameters: No of orders, Size of each order
                    stop_placing_buy_limit_orders = stop_placing_buy_limit_orders - 1
    except:
        actions.print_log("market_down exception")
        actions.print_exception()


# If market price goes up, buy Litecoin
def market_up(current_market_price):
    try:
        global initial_market_price, stop_placing_sell_limit_orders
        if actions.account_value()['currency2_total'] / current_market_price > 0.01:
            # Log allowed difference and initial and current market price for later reference
            actions.print_log("initial_market_price:                      ", initial_market_price)
            actions.print_log("current_market_price:                      ", current_market_price)
            actions.print_log("initial_market_price + allowed_difference: ", initial_market_price + allowed_difference)

            actions.print_log("Market Up. Buy currency1")
            actions.print_log("Cancelling currency2 held limit orders(if any)")
            actions.cancel_multiple_limit_orders(strategy1.my_greens)

            currency2_available = actions.account_value()['currency2_available']
            currency2_available = actions.truncate(currency2_available, 8)
            actions.print_log("currency2_available = ", currency2_available)
            is_buying = False   # To update account info if currency1 is bought.

            # Coinbase does not allow sell a huge number of coins in one transaction
            # and hence it should be split into smaller portions
            # Keep buying till total currency2 amount less than defined value
            # Keep buying only if the market price is still above initial market price + allowed_difference

            while ((currency2_available/current_market_price) > 0.01) and (actions.account_value()['current_market_price'] >= (initial_market_price + allowed_difference)):
                is_buying = True
                # Fetch the market_up_buy_size value from the settings file
                market_up_buy_size = Decimal(str(config.config_data['strategy2']['market_up_buy_size']))
                response = actions.execute_market_order('buy', product, market_up_buy_size)  # Sells upto a maximum of 10,000 LTC/BTC, not EUR. Coinbase API does not execute such big transactions regardless.
                track_order(response)
                currency2_available = Decimal(str(actions.fetch_account_by_id(actions.currency2_account_id)['available']))
            if is_buying:
                time.sleep(1)  # To give time for the server to tally the balances
                account_info = actions.account_value()
                initial_market_price = account_info['current_market_price']
                actions.print_log("initial_market_price: ", initial_market_price, "market_down at: ", initial_market_price - allowed_difference, "market_up at: ", initial_market_price + allowed_difference)

                # Place sell limit orders again with available currency1
                if stop_placing_sell_limit_orders > 0:  # To stop loss from overdoing this action
                    actions.print_log("Placing Sell Limit orders again after Market Buy. Number: ", stop_placing_sell_limit_orders)
                    strategy1.fetch_greens_and_reds()
                    strategy1.place_initial_sell_order(strategy1.initial_sell_quantity, strategy1.initial_sell_size)  # Parameters: No of orders, Size of each order
                    stop_placing_sell_limit_orders = stop_placing_sell_limit_orders - 1
    except:
        actions.print_log("market_up exception")
        actions.print_exception()


# Track buy and sell orders
def track_order(response):
    try:
        global sequence_number_counter, my_market_orders
        if 'id' in response:
            useful_data = {a: response[a] for a in
                           ['id', 'size', 'product_id', 'side', 'type', 'fill_fees', 'filled_size']}
            useful_data.update(
                {'settled': False, 'is_traded': False, 'sequence_number': 'Market' + str(sequence_number_counter)})
            if 'funds' in response:
                useful_data.update({'funds': response['funds']})
            my_market_orders.append(useful_data)
            sequence_number_counter = sequence_number_counter + Decimal('1')
            actions.print_log("my_market_orders: ", my_market_orders)
    except:
        actions.print_log("track_order exception")
        actions.print_exception()
