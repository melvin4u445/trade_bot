import actions, strategy1, strategy2, config, time

actions.print_log("\n\n****************** Restarting Execution ******************")


# Run Strategy1
def trade():
    strategy1.initial_run()
    while True:
        time.sleep(3)
        config.track_config_changes()
        strategy1.execute_strategy()


# Run Strategy2
def stop_loss():
    strategy2.initial_run()
    while True:
        config.track_config_changes()
        strategy2.execute_strategy()


# Run Strategy1 and Strategy2 combined
def trade_and_stop_loss():
    strategy2.initial_run()
    strategy1.initial_run()
    while True:
        # TODO: Make variable recognize when there is the change in the settings.ini file

        config.track_config_changes()
        strategy1.execute_strategy()
        strategy2.execute_strategy()


trade()
#stop_loss()
# trade_and_stop_loss()

