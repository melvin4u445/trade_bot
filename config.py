from configparser import ConfigParser
config_data = []


def read_config_data():
    # Read configuration file
    settings = ConfigParser()
    location = 'settings.ini'
    # Parse configuration file
    settings.read(location)

    temp_data = {s: dict(settings.items(s)) for s in settings.sections()}
    return temp_data


def track_config_changes():
    global config_data
    temp_data = read_config_data()

    # If the settings.ini file is read at the same time the changes are made, then there is a race condition that causes
    # to return an empty dictionary. This while condition ensures that the variable temp_data gets the new data.
    while temp_data == {}:
        #print("Reading config file")
        temp_data = read_config_data()
    # config_data gets the latest changes from temp_data
    if config_data != temp_data:
        config_data = temp_data
        print("config_data updated")
        print(config_data)


track_config_changes()