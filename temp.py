import actions

price = 100
size = 10
for i in range(0, 10):
    # Sell
    cost_including_fees = price * size
    fees = 0.0015 * cost_including_fees
    cost_excluding_fees = cost_including_fees - fees
    ## possible_sell_price = price + (fees/size) + 0.05
    possible_sell_price = price + (fees / size) + 1.05
    new_sell_price = possible_sell_price
    new_sell_size = size
    print("####################################")
    print("price", price)
    print("size", size)
    print("cost including fees", cost_including_fees)
    print("fees", fees)
    print("cost excluding fees", cost_excluding_fees)
    print("new_sell_price", new_sell_price)
    print("new_sell_size", new_sell_size)

    # Buy
    price = new_sell_price
    size = new_sell_size
    cost_including_fees = price * size
    fees = 0.0015 * cost_including_fees
    cost_excluding_fees = cost_including_fees - fees
    ## possible_buy_price = price - (fees / size) - 0.05
    possible_buy_price = price - (fees / size) - 1.05
    new_buy_price = possible_buy_price
    new_buy_size = cost_excluding_fees / new_buy_price
    print("####################################")
    print("price", price)
    print("size", size)
    print("cost including fees", cost_including_fees)
    print("fees", fees)
    print("cost excluding fees", cost_excluding_fees)
    print("new_buy_price", new_buy_price)
    print("new_buy_size", new_buy_size)

    price = new_buy_price
    size = new_buy_size
