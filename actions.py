import api_info
import requests, sys, traceback, os, time
import simplejson as json
import base64, hashlib, hmac
from requests.auth import AuthBase
from datetime import datetime
from decimal import Decimal, ROUND_DOWN

trade_history_file = 'trade_history.txt'
account_info_log = ""  # To log account info only when there is a change

api_base = api_info.api_base
api_key = api_info.api_key
api_secret = api_info.api_secret
passphrase = api_info.passphrase
product = api_info.product
currency1_account_id = api_info.currency1_account_id
currency2_account_id = api_info.currency2_account_id


class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


class GDAXRequestAuth(AuthBase):
    def __init__(self, api_key, secret_key, passphrase):
        self.api_key = api_key
        self.secret_key = secret_key
        self.passphrase = passphrase

    def __call__(self, request):
        # "Your timestamp must be within 30 seconds of the API service time,
        # or your request will be considered expired and rejected."
        # Adding a 30 second buffer to prevent some timestamp expiry issues
        timestamp = str(requests.get(api_base + '/time').json()['epoch'] + 30)
        message = timestamp + request.method + request.path_url + (request.body or '')
        hmac_key = base64.b64decode(self.secret_key)
        signature = hmac.new(hmac_key, message.encode('utf-8'), hashlib.sha256)
        signature_b64 = base64.b64encode(signature.digest())
        request.headers.update({
            'CB-ACCESS-SIGN': signature_b64,
            'CB-ACCESS-TIMESTAMP': timestamp,
            'CB-ACCESS-KEY': self.api_key,
            'CB-ACCESS-PASSPHRASE': self.passphrase,
            'Content-Type': 'application/json'
        })
        return request


def fetch_product_list():
    try:
        response = requests.get(api_base + '/products')
        formatted_json = json.dumps(response.json(), sort_keys=True, indent=4)
        print_log(formatted_json)
        print_log("List of available products", response.json())
        if response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
    except:
        print_log("fetch_product_list exception",response.json())
        print_exception()
    return response.json()


def fetch_current_market_data():
    try:
        response = requests.get(api_base + '/products/' + product + '/ticker')
        #print_log("Current market status", response.json())
        if response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception("")
    except:
        print_log("fetch_ticker_info exception", response.json())
        print_exception()
    return response.json()


def fetch_accounts():
    try:
        auth = GDAXRequestAuth(api_key, api_secret, passphrase)
        order_url = api_base + '/accounts'
        response = requests.get(order_url, auth=auth)
        print_log("List of available accounts", response.json())
        if response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
    except:
        print_log("fetch_accounts exception", response.json())
        print_exception()
    return response.json()


def fetch_account_by_id(account_id):
    try:
        auth = GDAXRequestAuth(api_key, api_secret, passphrase)
        order_url = api_base + '/accounts/' + account_id
        response = requests.get(order_url, auth=auth)
        #print_log("Account information", response.json())
        if response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
    except:
        try:
            if 'application/json' in response.headers.get('content-type'):
                print_log("fetch_accounts exception response.json()", response.json())
            else:
                print_log("fetch_accounts exception response", response)
            print_exception()
        except UnboundLocalError as error:
            print_log("UnboundLocalError: ", error)
    return response.json()


# Fetch the account information of various accounts. Returns a dictionary of those values
def account_value():
    global account_info_log
    currency1_info = fetch_account_by_id(currency1_account_id)
    currency2_info = fetch_account_by_id(currency2_account_id)
    market_info = fetch_current_market_data()
    if 'balance' in currency1_info and 'balance' in currency2_info and 'price' in market_info:
        # Check if the correct currency accounts are selected
        if currency1_info['currency'] == 'SHIB' and currency2_info['currency'] == 'EUR':
        # if currency1_info['currency'] == 'BTC' and currency2_info['currency'] == 'USD':
            currency1_total = Decimal(str(currency1_info['balance']))
            currency1_available = Decimal(str(currency1_info['available']))
            currency1_hold = Decimal(str(currency1_info['hold']))
            currency2_total = Decimal(str(currency2_info['balance']))
            currency2_available = Decimal(str(currency2_info['available']))
            currency2_hold = Decimal(str(currency2_info['hold']))
            current_market_price = Decimal(str(market_info['price']))
            currency2_value_of_account = (currency1_total * current_market_price) + currency2_total
            currency1_value_of_account = (currency2_total / current_market_price) + currency1_total

            # To avoid printing duplicate account info over and over again.Print only if there is something new
            new_account_info_log = "current_market_price:" + str(current_market_price) + "   currency2_value_of_account:"+ str(currency2_value_of_account) + "   currency1_value_of_account:"+ str(currency1_value_of_account) + "   currency1_total:"+ str(currency1_info['balance']) + "   currency1_available:" + str(currency1_info['available']) + "   currency1_hold:", str(currency1_info['hold']) + "   currency2_total:" + str(currency2_info['balance']) + "   currency2_available:"  + str(currency2_info['available']) + "   currency2_hold:", str(currency2_info['hold'])
            if account_info_log != new_account_info_log:
                account_info_log = new_account_info_log
                print_log("Account Info: ", account_info_log)

            account_info = {'current_market_price': current_market_price,
                            'currency2_value_of_account': currency2_value_of_account, 'currency1_value_of_account': currency1_value_of_account,
                            'currency1_total': currency1_total, 'currency1_available': currency1_available, 'currency1_hold': currency1_hold,
                            'currency2_total': currency2_total, 'currency2_available': currency2_available, 'currency2_hold': currency2_hold}
            return account_info
        else:
            print_log("Incorrect currencies")


def fetch_order_status(order_id):
    try:
        auth = GDAXRequestAuth(api_key, api_secret, passphrase)
        order_url = api_base + '/orders/' + order_id
        #print_log("order_url", order_url)
        response = requests.get(order_url, auth=auth)

        # print_log("response", response.json())
        #order_status = response.json()

        # Check if the order is cancelled manually
        if 'message' in response.json() and response.json()['message'] == 'NotFound':
            print_log("Order not found. Order id:", order_url)
        elif 'message' in response.json() and response.json()['message'] == 'request timestamp expired':
            print_log("auth:", auth)
            print_log("Request timestamp expired. Retrying")
            fetch_order_status(order_id)
        # elif response.status_code == 524:
        #     print_log("524: A timeout occurred")
        elif response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
        response_json = response.json()
        return response_json
    except json.decoder.JSONDecodeError:
        # Coinbase doesn't send a timeout response, so handling this error on the client side
        print_log("fetch_order_status JSONDecodeError exception")
        print_log("Fetch order status has timed out (probably)")
        print_log("order_url", order_url)
        time.sleep(5.0)  # To avoid too many requests to the server
        print_log("Slept 5 sec")
        response_json = {}
        return response_json
    except ConnectionResetError:
        print_log("fetch_order_status ConnectionResetError exception")
        print_log("ConnectionResetError: [Errno 54] Connection reset by peer")
        print_log("order_url", order_url)
        time.sleep(5.0)  # To avoid too many requests to the server
        print_log("Slept 5 sec")
        response_json = {}
        return response_json
    except:
        print_log("fetch_order_status exception")
        print_log("order_url", order_url)
        print_log("content-type", response.headers.get('content-type'))
        print_log("status code", response.status_code)
        print_log("headers", response.headers)
        print_log("content", response.content)
        print_log("encoding", response.encoding)
        print_log("request", response.request)
        print_log("url", response.url)
        print_log("text", response.text)
        if 'application/json' in response.headers.get('content-type'):
            print_log("fetch order status response1", response)
            print_log("fetch_order_status exception1", response.json())
        else:
            print_log("fetch order status response2", response)
        print_exception()
        response_json = {}
        return response_json



def execute_market_order(side, product, size):
    try:
        auth = GDAXRequestAuth(api_key, api_secret, passphrase)
        order_data = {
            'type': 'market',
            'side': side,
            'product_id': product,
            'size': size
        }
        response = requests.post(api_base + '/orders', data=json.dumps(order_data), auth=auth)
        print_log(side, "market order executed: ", response.json())
        if response.status_code != 200:
            #print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
    except:
        print_log("execute_market_order exception", response.json())
        print_exception()
    return response.json()


def fetch_candles():
    try:
        # 'start' & 'end' is time in ISO 8601
        # 'granularity' is the desired timeslice in seconds[60(1 min), 300(5 min), 900(15 min), 3600(1 hour), 21600(6 hours), 86400(1 day)]
        # Maximum number of data points for a single request is 300 candles
        order_data = {
            'granularity': 60
        }
        response = requests.get(api_base + '/products/' + product + '/candles', data=json.dumps(order_data))
        #print_log("Current market status", response.json())
        if response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception("")
    except:
        print_log("fetch_trades exception", response.json())
        print_exception()
    return response.json()

def execute_limit_order(side, product, price, size, time_in_force='GTC', cancel_after=None, post_only=True):
    try:
        auth = GDAXRequestAuth(api_key, api_secret, passphrase)
        order_data = {
            'type': 'limit',
            'side': side,
            'product_id': product,
            'price': str(price),
            'size': str(size),
            'time_in_force': time_in_force,
            'post_only': post_only
        }
        if 'time_in_force' == 'GTT':
            order_data['cancel_after'] = cancel_after
        if 'time_in_force' not in ['IOC', 'FOK']:
            order_data['post_only'] = post_only
        response = requests.post(api_base + '/orders', data=json.dumps(order_data), auth=auth)
        print_log(side, "limit order placed: ", response.json())
        if 'message' in response.json() and response.json()['message'] == 'Insufficient funds':
                print_log(response.json())
        elif 'message' in response.json() and response.json()['message'] == 'Post only mode':
                print_log(response.json(), "The planned price might have fluctuated and moved to the other side of the order book (greens/reds)")
                # ToDO: Recalculate and place order again
        elif response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
    except:
        print_log("execute_limit_order exception", response.json())
        print_exception()
    return response.json()


def cancel_limit_order(order_id):
    try:
        print_log("cancel_limit_order with order_id", order_id)
        auth = GDAXRequestAuth(api_key, api_secret, passphrase)
        response = requests.delete(api_base + '/orders/'+order_id, auth=auth)
        if response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
    except:
        print_log("cancel_limit_order exception", response.json())
        print_exception()
    return response.json()


def cancel_multiple_limit_orders(order_dict):
    for x in range(0, len(order_dict)):
        if order_dict[x]['settled'] is False:
            cancel_limit_order(order_dict[x]['id'])


def fetch_my_active_orders():
    try:
        auth = GDAXRequestAuth(api_key, api_secret, passphrase)
        response = requests.get(api_base + '/orders/', auth=auth)
        print_log("Listing my active orders", response.json())
        if response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
    except:
        print_log("fetch_my_active_orders exception", response.json())
        print_exception()
    return response.json()


def fetch_my_filled_orders(product):
    try:
        auth = GDAXRequestAuth(api_key, api_secret, passphrase)
        url = api_base + '/fills?product_id=' + product
        response = requests.get(url, auth=auth)
        my_fills = response.json()
        print_log("Listing my filled orders", my_fills)
        if response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
    except:
        print_log("fetch_my_filled_orders exception", response.json())
        print_exception()
    return my_fills


def fetch_order_book(product):
    try:
        auth = GDAXRequestAuth(api_key, api_secret, passphrase)
        level = 2  # Available levels are 1,2 and 3
        url = api_base + '/products/' + product + '/book/?level=' + str(level)
        response = requests.get(url, auth=auth)
        order_book = response.json()
        # print_log("Listing order book", order_book)
        if response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
    except:
        print_log("fetch_order_book exception", response.json())
        print_exception()
    return order_book


def fetch_fees():
    try:
        auth = GDAXRequestAuth(api_key, api_secret, passphrase)
        url = api_base + '/fees'
        response = requests.get(url, auth=auth)
        current_fees = response.json()
        print_log("Current fees", current_fees)
        if response.status_code != 200:
            print_log("Exception", response.json())
            raise Exception('Invalid GDAX Status Code: %d' % response.status_code)
    except:
        print_log("fetch_fees exception", response.json())
        print_exception()
    return current_fees

def fetch_greens(order_book):
    try:
        greens = order_book['bids']
    except:
        print_exception()
    return greens


def fetch_reds(order_book):
    try:
        reds = order_book['asks']
    except:
        print_exception()
    return reds


def truncate(d, places):
    return d.quantize(Decimal(10) ** -places, rounding=ROUND_DOWN)


def print_exception():
    traceback_log = traceback.format_exc()
    print_log(traceback_log.replace('\n', '\n                     '))


def print_log(*args):

    # Create a new log file every day to avoid having heavy sized logs
    log_location = os.path.dirname(os.getcwd()) + "/trade_bot_logs"
    log_file = log_location + "/trade_bot_" + time.strftime("%m.%d.%Y") + ".log"
    # Create folder if it doesn't exist
    if not os.path.exists(log_location):
        os.makedirs(log_location)
    #print(*args)
    with open(log_file, "a") as log:
        output = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        for arg in args:
            output = output + '  ' + str(arg)
        print(output)
        output = output + '\n'
        log.write(output)


def write_order_list_to_disk(sequence_number_counter, my_greens, my_reds):
    try:
        order_list = {'sequence_number_counter': sequence_number_counter, 'my_greens': my_greens, 'my_reds': my_reds}

        with open(trade_history_file, 'w') as file:
            json.dump(order_list, file)
    except:
        print_log("write_order_list_to_disk exception")
        print_exception()


def read_order_list_from_disk():
    try:
        # Create file if it doesn't exist
        if not os.path.exists(trade_history_file):
            open(trade_history_file, 'w')

        with open(trade_history_file, 'r') as file:
            # Initialise if the file is empty
            if os.stat(trade_history_file).st_size == 0:
                write_order_list_to_disk(1, [], [])

            # Read from the file
            order_list = json.load(file)
            if 'sequence_number_counter' in order_list:
                print_log("Fetching sequence_number_counter history")
                sequence_number_counter = Decimal(order_list['sequence_number_counter'])
            if 'my_greens' in order_list:
                print_log("Fetching my_greens history")
                my_greens = order_list['my_greens']
            if 'my_reds' in order_list:
                print_log("Fetching my_reds history")
                my_reds = order_list['my_reds']
            print_log("Logging history:", order_list)

        return sequence_number_counter, my_greens, my_reds
    except:
        print_log("read_order_list_from_disk exception")
        print_exception()

