# Strategy1: Place buy and sell limit orders on the first x prices in the order book

# ToDO: Sequence number counter is wrong
import actions, config
import time
from decimal import Decimal

product = actions.product
greens, reds, my_greens, my_reds = [], [], [], []
sequence_number_counter = Decimal('1')

# Fetch values from the settings file
initial_buy_quantity = int(config.config_data['strategy1']['initial_buy_quantity'])  # No of orders to place
initial_buy_size = Decimal(str(config.config_data['strategy1']['initial_buy_size']))  # Size of each order
initial_sell_quantity = int(config.config_data['strategy1']['initial_sell_quantity'])  # No of orders to place
initial_sell_size = Decimal(str(config.config_data['strategy1']['initial_sell_size']))  # Size of each order


def initial_run():
    try:
        global sequence_number_counter, my_greens, my_reds
        actions.fetch_accounts()
        actions.fetch_account_by_id(actions.currency1_account_id)
        actions.fetch_account_by_id(actions.currency2_account_id)
        fetch_greens_and_reds()
        sequence_number_counter, my_greens, my_reds = actions.read_order_list_from_disk()
        actions.print_log("Fetching account info before placing initial orders:", actions.account_value())
        actions.print_log("Also fetching trade history and restarting unfulfilled orders")
        place_initial_buy_order(initial_buy_quantity, initial_buy_size)  # Parameters: No of orders, Size of each order
        place_initial_sell_order(initial_sell_quantity, initial_sell_size)  # Parameters: No of orders, Size of each order
    except:
        actions.print_log("initial_run1 exception")
        actions.print_exception()

def execute_strategy():
    try:
        check_buy_order_status()
        check_sell_order_status()
        # Parameters: New Buy rate, New sell rate. Fetch values from the settings file
        buy_rate = Decimal(str(config.config_data['strategy1']['buy_rate']))
        sell_rate = Decimal(str(config.config_data['strategy1']['sell_rate']))
        trade_my_reds(buy_rate)
        trade_my_greens(sell_rate)
    except:
        actions.print_log("execute_strategy1 exception")
        actions.print_exception()


def fetch_greens_and_reds():
    try:
        global greens, reds
        order_book = actions.fetch_order_book(product)
        greens = order_book['bids']
        reds = order_book['asks']
    except:
        actions.print_log("fetch_greens_and_reds exception")
        actions.print_exception()

def place_initial_buy_order(max_buy, size):
    try:
        global greens, my_greens, sequence_number_counter, my_reds
        for x in range(0, len(greens)):
            if x < max_buy:
                response = actions.execute_limit_order('buy', product, greens[x][0], size)
                actions.account_value()  # Fetching information about each account
                if 'id' in response:
                    useful_data = {a: response[a] for a in ['id', 'price', 'size', 'product_id', 'side', 'type', 'fill_fees', 'executed_value']}
                    useful_data.update({'settled': False, 'is_traded': False, 'sequence_number': sequence_number_counter})
                    my_greens.append(useful_data)
                    sequence_number_counter = sequence_number_counter + Decimal('1')
                    actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
                else:
                    actions.print_log("place_initial_buy_order error", response)
                    break
            else:
                break
        # actions.fetch_accounts()
        actions.fetch_account_by_id(actions.currency1_account_id)
        actions.fetch_account_by_id(actions.currency2_account_id)
    except:
        actions.print_log("place_initial_buy_order exception")
        actions.print_exception()

def place_initial_sell_order(max_sell, size):
    try:
        global reds, my_reds, sequence_number_counter, my_greens
        for x in range(0, len(reds)):
            if x < max_sell:
                response = actions.execute_limit_order('sell', product, reds[x][0], size)
                actions.account_value()  # Fetching information about each account
                if 'id' in response:
                    useful_data = {a: response[a] for a in ['id', 'price', 'size', 'product_id', 'side', 'type', 'fill_fees', 'executed_value']}
                    useful_data.update({'settled': False, 'is_traded': False, 'sequence_number': sequence_number_counter})
                    my_reds.append(useful_data)
                    sequence_number_counter = sequence_number_counter + Decimal('1')
                    actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
                else:
                    actions.print_log("place_initial_sell_order error", response)
                    break
            else:
                break
        # actions.fetch_accounts()
        actions.fetch_account_by_id(actions.currency1_account_id)
        actions.fetch_account_by_id(actions.currency2_account_id)
    except:
        actions.print_log("place_initial_sell_order exception")
        actions.print_exception()


def check_buy_order_status():
    try:
        global my_greens
        for x in range(0, len(my_greens)):
            if my_greens[x]['product_id'] == actions.product and my_greens[x]['settled'] is False:
                order_status = actions.fetch_order_status(my_greens[x]['id'])
                time.sleep(0.05)  # To avoid too many requests to the server
                if 'settled' in order_status:
                    if order_status['settled'] is True:
                        my_greens[x].update({'fill_fees': order_status['fill_fees'], 'executed_value': order_status['executed_value'], 'settled': order_status['settled']})
                        actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
                        actions.print_log("====================================")
                        actions.print_log("my_greens", my_greens)
                        actions.print_log("Updated buy order", my_greens[x])
                if 'message' in order_status and order_status['message'] == 'NotFound':
                    actions.print_log("Marking order as cancelled")
                    my_greens[x].update({'settled': 'cancelled'})
                    actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)

    except:
        actions.print_log("check_buy_order_status exception")
        actions.print_exception()

def check_sell_order_status():
    try:
        global my_reds
        for x in range(0, len(my_reds)):
            if my_reds[x]['product_id'] == actions.product and my_reds[x]['settled'] is False:
                order_status = actions.fetch_order_status(my_reds[x]['id'])
                time.sleep(0.05) # To avoid too many requests to the server
                if 'settled' in order_status:
                    if order_status['settled'] is True:
                        my_reds[x].update({'fill_fees': order_status['fill_fees'], 'executed_value': order_status['executed_value'], 'settled': order_status['settled']})
                        actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
                        actions.print_log("====================================")
                        actions.print_log("my_reds", my_reds)
                        actions.print_log("Updated sell order", my_reds[x])
                if 'message' in order_status and order_status['message'] == 'NotFound':
                    actions.print_log("Marking order as cancelled")
                    my_reds[x].update({'settled': 'cancelled'})
                    actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
    except:
        actions.print_log("check_sell_order_status exception")
        actions.print_exception()

def trade_my_reds(buy_rate):
    try:
        global my_greens, my_reds, sequence_number_counter

        # Place counter-buy orders for filled sell orders
        for x in range(0, len(my_reds)):
            if my_reds[x]['product_id'] == actions.product and my_reds[x]['settled'] is True and my_reds[x]['is_traded'] is False:
                price = Decimal(str(my_reds[x]['price']))  # Rate per coin
                size = Decimal(str(my_reds[x]['size']))
                fees = Decimal(str(my_reds[x]['fill_fees']))
                # TODO: Check if 'executed_value' shows the price including fees in my_reds
                cost_including_fees = Decimal(str(my_reds[x]['executed_value']))
                sequence_number = my_reds[x]['sequence_number']
                buy_rate = Decimal(str(buy_rate))

                possible_buy_price = price - (2*(fees/size)) - buy_rate

                actions.print_log("####################################")
                actions.print_log("Sequence number", sequence_number)
                actions.print_log("Parent order id my_reds[x]['id']", my_reds[x]['id'])
                actions.print_log("price", price)
                actions.print_log("size", size)
                actions.print_log("cost including fees", cost_including_fees)
                actions.print_log("fees", fees)
                actions.print_log("sequence_number", sequence_number)
                actions.print_log("possible_buy_price", possible_buy_price)

                response = {'id': 0}
                while 'id' in response and response['id'] == 0:  # To make sure the order is placed(due to price fluctuations)
                    actions.print_log("inside trade_my_reds while")
                    fetch_greens_and_reds()  # Automatically updates the global reds and greens variables
                    actions.print_log("Decimal(str(greens[0][0]))", Decimal(str(greens[0][0])))
                    if possible_buy_price <= Decimal(str(greens[0][0])):
                        new_buy_price = possible_buy_price
                    elif possible_buy_price > Decimal(str(greens[0][0])):
                        new_buy_price = Decimal(str(greens[0][0]))
                    #new_buy_size = actual_amount_available / new_buy_price
                    new_buy_size = size
                    actions.print_log("new_buy_price", new_buy_price)
                    actions.print_log("new_buy_size", new_buy_size)
                    new_buy_price = actions.truncate(new_buy_price, 7)
                    new_buy_size = actions.truncate(new_buy_size, 8)
                    actions.print_log("new_buy_price truncated", new_buy_price)
                    actions.print_log("new_buy_size truncated", new_buy_size)
                    actions.print_log("cost for purchase", new_buy_price * new_buy_size)
                    # To make sure that the funds from the previous sales are released
                    available_amount = Decimal(str(actions.fetch_account_by_id(actions.currency2_account_id)['available']))
                    try_times = 2
                    while (available_amount < (new_buy_size * new_buy_price)) and try_times > 0:
                        actions.print_log("Expected", new_buy_size * new_buy_price, "currency2 in account but available is", available_amount)
                        actions.print_log("Was the amount allocated manually to another trade?")
                        time.sleep(0.5)
                        available_amount = Decimal(str(actions.fetch_account_by_id(actions.currency2_account_id)['available']))
                        actions.print_log(actions.fetch_account_by_id(actions.currency2_account_id))
                        actions.print_log(actions.fetch_account_by_id(actions.currency1_account_id))
                        try_times = try_times - 1
                    else:
                        # Insufficient funds to place order. Mark the order's 'is_traded' value as 'failed'
                        if try_times == 0:
                            my_reds[x].update({'is_traded': 'failed'})
                            actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
                            actions.print_log("my_reds['is_traded'] updated as 'failed' ", my_reds)
                            response = {'id': 1}
                    if my_reds[x]['is_traded'] != 'failed':
                        actions.account_value()  # Fetching information about each account
                        response = actions.execute_limit_order('buy', product, new_buy_price, new_buy_size)
                        actions.account_value()  # Fetching information about each account
                        if 'id' in response and 'price' in response:
                            my_reds[x].update({'is_traded': True})
                            actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
                            actions.print_log("my_reds updated after placing counter-order", my_reds)
                            # Add the new order into the my_greens list
                            useful_data = {a: response[a] for a in
                                           ['id', 'price', 'size', 'product_id', 'side', 'type', 'fill_fees',
                                            'executed_value'] if a in response}
                            useful_data.update({'settled': False, 'is_traded': False, 'sequence_number': sequence_number + Decimal('0.1')})
                            my_greens.append(useful_data)
                            actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
                            actions.print_log("New order added to my_greens", my_greens)
                            actions.print_log("****************************")
                            # actions.fetch_accounts()
                            actions.fetch_account_by_id(actions.currency1_account_id)
                            actions.fetch_account_by_id(actions.currency2_account_id)
                        else:
                            actions.print_log(response)
    except:
        actions.print_log("trade_my_greens exception")
        actions.print_exception()


def trade_my_greens(sell_rate):
    try:
        # Place counter-sell orders for filled buy orders
        for x in range(0, len(my_greens)):
            if my_greens[x]['product_id'] == actions.product and my_greens[x]['settled'] is True and my_greens[x]['is_traded'] is False:
                price = Decimal(str(my_greens[x]['price']))  # Rate per coin
                size = Decimal(str(my_greens[x]['size']))
                fees = Decimal(str(my_greens[x]['fill_fees']))
                # TODO: Check if 'executed_value' shows the price including fees in my_greens
                cost_including_fees = Decimal(str(my_greens[x]['executed_value']))
                sequence_number = my_greens[x]['sequence_number']
                sell_rate = Decimal(str(sell_rate))

                amount_after_fees = cost_including_fees - fees
                possible_sell_price = price + (fees/size) + sell_rate

                actions.print_log("####################################")
                actions.print_log("Sequence number", sequence_number)
                actions.print_log("Parent order id my_greens[x]['id']", my_greens[x]['id'])
                actions.print_log("price", price)
                actions.print_log("size", size)
                actions.print_log("cost including fees", cost_including_fees)
                actions.print_log("fees", fees)
                actions.print_log("sequence_number", sequence_number)
                actions.print_log("cost excluding fees", amount_after_fees)
                actions.print_log("possible_sell_price", possible_sell_price)

                response = {'id': 0}
                while 'id' in response and response['id'] == 0:  # To make sure the order is placed(due to price fluctuations)
                    actions.print_log("inside trade_my_greens while")
                    fetch_greens_and_reds()  # Automatically updates the global reds and greens variables
                    actions.print_log("Decimal(str(reds[0][0])", Decimal(str(reds[0][0])))
                    if possible_sell_price >= Decimal(str(reds[0][0])):
                        new_sell_price = possible_sell_price
                    elif possible_sell_price < Decimal(str(reds[0][0])):
                        new_sell_price = Decimal(str(reds[0][0]))
                    '''
                    new_sell_size = cost_including_fees/new_sell_price
                    if new_sell_size < 0.1:  # CoinBase does not allow limit order below 0.1 LTC
                        # Setting new_sell_size = size will cause the fees to be deducted from the EUR account
                        new_sell_size = size
                    '''
                    new_sell_size = size
                    actions.print_log("new_sell_price", new_sell_price)
                    actions.print_log("new_sell_size", new_sell_size)
                    new_sell_price = actions.truncate(new_sell_price, 7)
                    new_sell_size = actions.truncate(new_sell_size, 8)
                    actions.print_log("new_sell_price truncated", new_sell_price)
                    actions.print_log("new_sell_size truncated", new_sell_size)
                    actions.print_log("cost for purchase", new_sell_price * new_sell_size)
                    # To make sure that the funds from the previous sales are released
                    available_amount = Decimal(str(actions.fetch_account_by_id(actions.currency1_account_id)['available']))
                    try_times = 2
                    while (available_amount < new_sell_size) and try_times > 0:
                        actions.print_log("Expected", new_sell_size, "coins in account but available is", available_amount)
                        actions.print_log("Was the amount allocated manually to another trade?")
                        time.sleep(0.5)
                        available_amount = Decimal(str(actions.fetch_account_by_id(actions.currency1_account_id)['available']))
                        actions.print_log(actions.fetch_account_by_id(actions.currency2_account_id))
                        actions.print_log(actions.fetch_account_by_id(actions.currency1_account_id))
                        try_times = try_times - 1
                    else:
                        # Insufficient funds to place order. Mark the order's 'is_traded' value as 'failed'
                        if try_times == 0:
                            my_greens[x].update({'is_traded': 'failed'})
                            actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
                            actions.print_log("my_greens['is_traded'] updated as 'failed' ", my_greens)
                            response = {'id': 1}
                    if my_greens[x]['is_traded'] != 'failed':
                        actions.account_value()  # Fetching information about each account
                        response = actions.execute_limit_order('sell', product, new_sell_price, new_sell_size)
                        actions.account_value()  # Fetching information about each account
                        if 'id' in response and 'price' in response:
                            my_greens[x].update({'is_traded': True})
                            actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
                            actions.print_log("my_greens updated after placing counter-order", my_greens)
                            # Add the new order into the my_reds list
                            useful_data = {a: response[a] for a in
                                           ['id', 'price', 'size', 'product_id', 'side', 'type', 'fill_fees',
                                            'executed_value'] if a in response}
                            useful_data.update({'settled': False, 'is_traded': False, 'sequence_number': sequence_number + Decimal('0.1')})
                            my_reds.append(useful_data)
                            actions.write_order_list_to_disk(sequence_number_counter, my_greens, my_reds)
                            actions.print_log("New order added to my_reds", my_reds)
                            actions.print_log("****************************")
                            # actions.fetch_accounts()
                            actions.fetch_account_by_id(actions.currency1_account_id)
                            actions.fetch_account_by_id(actions.currency2_account_id)
                        else:
                            actions.print_log(response)
    except:
        actions.print_log("trade_my_greens exception")
        actions.print_exception()